upstream the work in this repo, leverage craft also to build the apps themselves

see e.g. digikam for custom apprun and appimage package steps

upstream craft blueprints into:
https://invent.kde.org/packaging/craft-blueprints-community

existing docker images for reuse (sles15 for qt6 eventually):
- https://invent.kde.org/sysadmin/ci-images/-/tree/master/centos7-craft?ref_type=heads
- https://invent.kde.org/sysadmin/ci-images/-/tree/master/sles15-craft?ref_type=heads

kde registry:
- https://invent.kde.org/sysadmin/ci-images/container_registry
- invent-registry.kde.org/sysadmin/ci-images/centos7-craft:latest

inspiration for external ci:
- https://github.com/owncloud/client/blob/master/.github/workflows/main.yml
- https://gitlab.com/mattbas/glaxnimate/-/blob/master/.gitlab/ci/craft-appimage.yml?ref_type=heads
- https://invent.kde.org/sysadmin/ci-utilities/-/tree/master/gitlab-templates
