#!/bin/sh

cd $(dirname $0)

cmd="$1"

if [ -z "$cmd" ]; then
    cmd=bash
else
    shift 1
fi

docker run --rm -it \
    $RUN_IN_DOCKER_EXTRA_ARGS \
    -v $PWD/craft-root:/opt/craft/ \
    --device /dev/fuse \
    --cap-add SYS_ADMIN \
    kf5-appimage-centos7 $cmd "$@"
