#!/bin/sh

cd $(dirname $0)
docker build --no-cache --ulimit nofile=1024:262144  \
    --rm -t kf5-appimage-centos7 .
