#!/bin/sh

# wipes the craft-root folder and re-installs craft

set -e

cd $(dirname $0)

rm -Rf craft-root
mkdir craft-root

./run_in_docker.sh /bin/sh -c 'curl -o /tmp/craft.py https://raw.githubusercontent.com/KDE/craft/master/setup/CraftBootstrap.py; /opt/pyenv/shims/python3 /tmp/craft.py --prefix /opt/craft/ --branch 23.07 --use-defaults'

cp craft.sh.template craft-root/craft.sh

./run_craft_in_docker.sh --set revision=23.07 craft-core
