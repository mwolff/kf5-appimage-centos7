# kf5-appimage-baseimage

This repository hosts a collection of scripts to generate a docker image for usage
in appimage creation that's compatible with CentOS 7.

## usage

To build the docker image and populate the cache in `craft-root/cache`:

```
./docker/build_docker.sh
./run_craft_in_docker.sh craft craft kde/frameworks
```

To drop into a shell in the docker image for custom stuff:

```
./run_in_docker.sh
```
