#!/bin/sh

set -e

cd $(dirname $0)

../../run_craft_in_docker.sh linuxdeploy appimagetool

cp build.sh shared/
RUN_IN_DOCKER_EXTRA_ARGS="-v $PWD/shared:/opt/ctf2ctf" \
    ../../run_in_docker.sh /opt/ctf2ctf/build.sh
