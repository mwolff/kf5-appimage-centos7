#!/bin/bash

set -e

source /opt/craft/craft/craftenv.sh

cd $(dirname $0)

export ROOT="$PWD"
export CRAFT_PREFIX=/opt/craft/

function git_init()
{
    dir=$1
    url=$2
    if [ ! -d "$dir" ]; then
        git clone "$url" "$dir"
    fi
    cd "$dir"
    branch="$3"
    if [ -z "$branch" ]; then
        branch="master"
    fi
    git checkout "$branch"
    git pull --rebase
    git submodule update --init --recursive
}

function build_babeltrace()
{
    git_init babeltrace https://github.com/kdab/babeltrace.git stable-1.5
    git clean -xfd .
    ./bootstrap
    ./configure --disable-debug-info --prefix="$ROOT/prefix"
    make -j
    make install
}

function build_ctf2ctf()
{
    git_init ctf2ctf https://github.com/KDAB/ctf2ctf.git

    rm -Rf build
    mkdir build
    cd build

    PREFIX=/usr
    cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo -GNinja \
        "-DCMAKE_PREFIX_PATH=$CRAFT_PREFIX" \
        "-DBabeltrace_PATH_HINT=$ROOT/prefix" \
        "-DCMAKE_INSTALL_PREFIX=$PREFIX" ..

    ninja
    mkdir -p "appdir/$PREFIX/bin"
    cp ctf2ctf "appdir/$PREFIX/bin"

    curl -o ctf2ctf.svg "https://invent.kde.org/frameworks/breeze-icons/-/raw/master/icons/apps/64/utilities-terminal.svg?inline=false"
    linuxdeploy-x86_64.AppImage --create-desktop-file -e "./appdir/$PREFIX/bin/ctf2ctf" \
        --appdir="./appdir" --icon-file=ctf2ctf.svg

    # Actually create the final image
    appimagetool-x86_64.AppImage ./appdir ../../ctf2ctf-$(git describe --always)-x86_64.AppImage
}

pushd .
build_babeltrace
popd

pushd .
build_ctf2ctf
popd
