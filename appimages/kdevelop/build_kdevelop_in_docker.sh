#!/bin/sh

cd $(dirname $0)

../../run_craft_in_docker.sh linuxdeploy appimagetool kio-extras kdevelop kdev-python kdev-php

cp build.sh shared/
RUN_IN_DOCKER_EXTRA_ARGS="-v $PWD/shared:/opt/kdevelop" \
    ../../run_in_docker.sh "/opt/craft/craft.sh --run /opt/kdevelop/build.sh"
