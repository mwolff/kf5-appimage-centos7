#!/bin/bash

set -e

source /opt/craft/craft/craftenv.sh

cd $(dirname $0)

# export also for usage linuxdeploy
export VERSION=$(/opt/craft/craft.sh --print-installed | grep kdevelop/kdevelop | awk '{print $3}')
CRAFT_PREFIX=/opt/craft/
ROOT=$PWD

function build_exec_wrapper()
{
    rm -Rf appimage-exec-wrapper
    git clone https://invent.kde.org/brauch/appimage-exec-wrapper.git
    pushd appimage-exec-wrapper
    make clean
    make
    popd
}

function package_kdevelop()
{
    echo "packaging kdevelop v$VERSION"

    rm -Rf appdir
    mkdir -p "appdir/usr"

    for app in kdevelop kdev-php kdev-python; do
        build_dir="$CRAFT_PREFIX/build/extragear/kdevelop/$app/image-RelWithDebInfo-$VERSION"
        cp -vR "$build_dir/"* "appdir/usr/"
    done

    # include the clang builtin dir
    LLVM_VERSION=$(ls "$CRAFT_PREFIX/lib/clang/")
    cp -vR "$CRAFT_PREFIX/lib/clang" "appdir/usr/lib/"

    # include necessary python bits for kdev-python
    cp -vR "/usr/lib64/python3.6" "appdir/usr/lib/"
    rm -Rf appdir/usr/lib/python3.6/{test,config-3.5m,__pycache__,site-packages,lib-dynload,distutils,idlelib,unittest,tkinter,ensurepip}

    # include various kde plugins we may need
    pluginsdir="appdir/usr/plugins/"
    mkdir -p "$pluginsdir/kf5"
    for d in kwindowsystem kio urifilters sonnet kiod kded ktranscript.so kguiaddons purpose kfileitemaction; do
        cp -vR "$CRAFT_PREFIX/plugins/kf5/$d" "$pluginsdir/kf5/"
    done
    mkdir -p "$pluginsdir/kf5/parts"
    for p in katepart.so; do
        cp -vR "$CRAFT_PREFIX/plugins/kf5/parts/$p" "$pluginsdir/kf5/parts/"
    done

    # some more kio bits
    mkdir -p "appdir/usr/libexec/kf5"
    cp "$CRAFT_PREFIX/lib/libexec/kf5/kioslave5" "appdir/usr/libexec/kf5/"

    # include breeze icons
    mkdir -p "appdir/usr/share/AppRun.wrapped"
    cp -v "$CRAFT_PREFIX/share/icons/breeze/breeze-icons.rcc" "appdir/usr/share/AppRun.wrapped/icontheme.rcc"

    # Install some colorschemes
    src_dir="$CRAFT_PREFIX/build/extragear/kdevelop/kdevelop/work/kdevelop-$VERSION"
    "$src_dir/release-scripts/install_colorschemes.py" "appdir/usr/share"

    kdevpluginsversion=$(ls $pluginsdir/kdevplatform)

    # first assemble everything we may need
    linuxdeploy-x86_64.AppImage --appdir "appdir" --plugin qt \
        -e "appdir/usr/bin/kdevelop" \
        --deploy-deps-only="$pluginsdir/kdevplatform/$kdevpluginsversion" \
        --deploy-deps-only="$pluginsdir/kf5/sonnet" \
        -l "$CRAFT_PREFIX/lib/libz.so.1" \
        -l "/usr/lib64/libharfbuzz.so.0" \
        -l "$ROOT/appimage-exec-wrapper/exec.so" \
        -d "appdir/usr/share/applications/org.kde.kdevelop.desktop" \
        -i "$src_dir/app/icons/512-apps-kdevelop.png" --icon-filename=kdevelop

    # reduce size, remove debug files
    # TODO keep these somewhere to allow debugging
    find "appdir/" -name "*.sym" -delete -or -name "*.debug" -delete

    # exclude libdbus-1.so, otherwise UDisk support in solid seems to be broken
    rm "appdir/usr/lib/libdbus-1.so.3"

    # remove webkit, not sure why it's getting pulled in
    rm "appdir/usr/libexec/"{QtWebStorageProcess,QtWebNetworkProcess,QtWebProcess}
    rm "appdir/usr/lib/"{libQt5WebKit.so.5,libQt5WebKitWidgets.so.5}

    # adapt the appdir to use the exec wrapper and set some env vars
    sed -i '/^source .*/a source "$this_dir/apprun-hooks/kdevelop.sh"' appdir/AppRun
    cat > appdir/apprun-hooks/kdevelop.sh << EOF
export LD_PRELOAD="\$this_dir/usr/lib/exec.so"
export APPDIR="\$this_dir"

export APPIMAGE_ORIGINAL_QML2_IMPORT_PATH="\$QML2_IMPORT_PATH"
export APPIMAGE_ORIGINAL_LD_LIBRARY_PATH="\$LD_LIBRARY_PATH"
export APPIMAGE_ORIGINAL_QT_PLUGIN_PATH="\$QT_PLUGIN_PATH"
export APPIMAGE_ORIGINAL_XDG_DATA_DIRS="\$XDG_DATA_DIRS"
export APPIMAGE_ORIGINAL_PATH="\$PATH"
export APPIMAGE_ORIGINAL_PYTHONHOME="\$PYTHONHOME"
export APPIMAGE_ORIGINAL_PYTHONPATH="\$PYTHONPATH"

export QML2_IMPORT_PATH="\$this_dir/usr/qml:\$QML2_IMPORT_PATH"
export LD_LIBRARY_PATH="\$this_dir/usr/lib/:\$LD_LIBRARY_PATH"
export QT_PLUGIN_PATH="\$this_dir/usr/plugins/"
export XDG_DATA_DIRS="\$this_dir/usr/share/:\$XDG_DATA_DIRS"
export PATH="\$this_dir/usr/bin:\$PATH"
export KDE_FORK_SLAVES=1
export PYTHONHOME="\$this_dir/usr/"
export PYTHONPATH="\$PYTHONHOME/lib/python3.6"

export APPIMAGE_STARTUP_QML2_IMPORT_PATH="\$QML2_IMPORT_PATH"
export APPIMAGE_STARTUP_LD_LIBRARY_PATH="\$LD_LIBRARY_PATH"
export APPIMAGE_STARTUP_QT_PLUGIN_PATH="\$QT_PLUGIN_PATH"
export APPIMAGE_STARTUP_XDG_DATA_DIRS="\$XDG_DATA_DIRS"
export APPIMAGE_STARTUP_PATH="\$PATH"
export APPIMAGE_STARTUP_PYTHONHOME="\$PYTHONHOME"
export APPIMAGE_STARTUP_PYTHONPATH="\$PYTHONPATH"

export KDEV_CLANG_BUILTIN_DIR="\$this_dir/usr/lib/clang/${LLVM_VERSION}/include"
export KDEV_DISABLE_PLUGINS="KDevWelcomePage"
EOF

    # then actually create the appimage
    appimagetool-x86_64.AppImage "./appdir" KDevelop-$VERSION-x86_64.AppImage
}

build_exec_wrapper
package_kdevelop
