#!/bin/bash

set -e

source /opt/craft/craft/craftenv.sh

cd $(dirname $0)

export ROOT=$PWD

function git_init()
{
    dir=$1
    url=$2
    if [ ! -d "$dir" ]; then
        git clone "$url" "$dir"
    fi
    cd "$dir"
    git fetch
    git checkout 1.5
    git pull --rebase
    git submodule update --init --recursive
}

function build_heaptrack()
{
    git_init heaptrack https://invent.kde.org/sdk/heaptrack.git

    git clean -xfd .
    mkdir build
    cd build

    CRAFT_PREFIX=/opt/craft/
    PREFIX=/usr
    cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo -GNinja \
        "-DCMAKE_PREFIX_PATH=$CRAFT_PREFIX" \
        -DAPPIMAGE_BUILD=ON "-DCMAKE_INSTALL_PREFIX=$PREFIX" ..

    DESTDIR=appdir ninja install

    # FIXME: Do in CMakeLists.txt
    mkdir -p "appdir/$PREFIX/share/applications/"
    cp ../src/analyze/gui/org.kde.heaptrack.desktop "appdir/$PREFIX/share/applications/"

    # Ensure we prefer the bundled libs also when calling dlopen, cf.: https://github.com/KDAB/hotspot/issues/89
    mv "./appdir/$PREFIX/bin/heaptrack_gui" "./appdir/$PREFIX/bin/heaptrack_gui_bin"
    cat << WRAPPER_SCRIPT > ./appdir/$PREFIX/bin/heaptrack_gui
#!/bin/bash
f="\$(readlink -f "\${0}")"
d="\$(dirname "\$f")"
unset QT_PLUGIN_PATH
LD_LIBRARY_PATH="\$d/../lib:\$LD_LIBRARY_PATH" "\$d/heaptrack_gui_bin" "\$@"
WRAPPER_SCRIPT
    chmod +x ./appdir/$PREFIX/bin/heaptrack_gui

    # include breeze icons
    mkdir -p "appdir/$PREFIX/share/icons/breeze"
    cp -v "$CRAFT_PREFIX/share/icons/breeze/breeze-icons.rcc" "appdir/$PREFIX/share/icons/breeze/"

    # use the shell script as AppRun entry point
    # also make sure we find the bundled zstd
    cat << WRAPPER_SCRIPT > ./appdir/AppRun
#!/bin/bash
f="\$(readlink -f "\${0}")"
d="\$(dirname "\$f")"
bin="\$d/$PREFIX/bin"
PATH="\$PATH:\$bin" "\$bin/heaptrack" "\$@"
WRAPPER_SCRIPT
    chmod +x ./appdir/AppRun

    linuxdeploy-x86_64.AppImage --appdir appdir --plugin qt \
        -e "./appdir/$PREFIX/bin/heaptrack_gui_bin" \
        -l "$CRAFT_PREFIX/lib/libz.so.1" \
        -l /usr/lib64/libharfbuzz.so.0 \
        -i ../src/analyze/gui/512-heaptrack_app_icon.png --icon-filename=heaptrack \
        -d "./appdir/$PREFIX/share/applications/org.kde.heaptrack.desktop"

    # fixup location of some libs to allow them to be found properly
    mv "./appdir/$PREFIX/lib/libunwind.so.8" "./appdir/$PREFIX/lib/heaptrack/"

    # Actually create the final image
    appimagetool-x86_64.AppImage ./appdir ../../heaptrack-$(git describe)-x86_64.AppImage
}

pushd .
build_heaptrack
popd
