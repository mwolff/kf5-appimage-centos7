#!/bin/sh

set -e

cd $(dirname $0)

../../run_craft_in_docker.sh \
    kde/frameworks/tier1/kcoreaddons \
    kde/frameworks/tier1/ki18n \
    kde/frameworks/tier1/kitemmodels \
    kde/frameworks/tier1/threadweaver \
    kde/frameworks/tier3/kconfigwidgets \
    kde/frameworks/tier3/kio \
    kde/frameworks/tier3/kiconthemes \
    extragear/kdiagram \
    boost-headers boost-system boost-program-options boost-iostreams boost-filesystem boost-container \
    libs/libunwind libzstd \
    linuxdeploy appimagetool elfutils

cp build.sh shared/
RUN_IN_DOCKER_EXTRA_ARGS="-v $PWD/shared:/opt/heaptrack" \
    ../../run_in_docker.sh /opt/heaptrack/build.sh
