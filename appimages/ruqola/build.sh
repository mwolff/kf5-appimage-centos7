#!/bin/sh

set -e

cd $(dirname $0)

# export also for usage linuxdeploy
export VERSION=master
CRAFT_PREFIX=/opt/craft/
ROOT=$PWD

function package_ruqola()
{
    echo "packaging ruqola $VERSION"

    rm -Rf appdir
    mkdir -p "appdir/usr"

    build_dir="$CRAFT_PREFIX/build/kde/unreleased/ruqola/image-RelWithDebInfo-$VERSION"
    cp -vR "$build_dir/"* "appdir/usr/"

    # include various kde plugins we may need
    pluginsdir="appdir/usr/plugins/"
    mkdir -p "$pluginsdir/kf5"
    for d in kwindowsystem kio urifilters sonnet kiod kded ktranscript.so kguiaddons purpose kfileitemaction; do
        cp -vR "$CRAFT_PREFIX/plugins/kf5/$d" "$pluginsdir/kf5/"
    done

    # some more kio bits
    mkdir -p "appdir/usr/libexec/kf5"
    cp "$CRAFT_PREFIX/lib/libexec/kf5/kioslave5" "appdir/usr/libexec/kf5/"

    # include breeze icons
    mkdir -p "appdir/usr/share/AppRun.wrapped"
    cp -v "$CRAFT_PREFIX/share/icons/breeze/breeze-icons.rcc" "appdir/usr/share/AppRun.wrapped/icontheme.rcc"

    # first assemble everything we may need
    linuxdeploy-x86_64.AppImage --appdir "appdir" --plugin qt \
        -e "appdir/usr/bin/ruqola" \
        --deploy-deps-only="$pluginsdir/kf5/sonnet" \
        -l "$CRAFT_PREFIX/lib/libz.so.1" \
        -l "/usr/lib64/libharfbuzz.so.0" \
        -d "appdir/usr/share/applications/org.kde.ruqola.desktop" \
        -i "appdir/usr/share/icons/hicolor/48x48/apps/ruqola.png" --icon-filename=ruqola

    # reduce size, remove debug files
    # TODO keep these somewhere to allow debugging
    find "appdir/" -name "*.sym" -delete -or -name "*.debug" -delete

    # exclude libdbus-1.so, otherwise UDisk support in solid seems to be broken
    rm "appdir/usr/lib/libdbus-1.so.3"

    # then actually create the appimage
    appimagetool-x86_64.AppImage "./appdir" ruqola-$VERSION-x86_64.AppImage
}

package_ruqola
