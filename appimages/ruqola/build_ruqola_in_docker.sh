#!/bin/sh

cd $(dirname $0)

../../run_craft_in_docker.sh linuxdeploy appimagetool ruqola

cp build.sh shared/
RUN_IN_DOCKER_EXTRA_ARGS="-v $PWD/shared:/opt/ruqola" \
    ../../run_in_docker.sh "/opt/craft/craft.sh --run /opt/ruqola/build.sh"
