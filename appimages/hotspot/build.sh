#!/bin/bash

set -e

source /opt/craft/craft/craftenv.sh

cd $(dirname $0)

export ROOT=$PWD
export RUSTUP_HOME=$PWD/rust
export CARGO_HOME=$PWD/.cargo

function git_init()
{
    dir=$1
    url=$2
    branch=$3
    if [ ! -d "$dir" ]; then
        git clone "$url" "$dir"
    fi
    cd "$dir"
    git checkout "$branch"
    git submodule update --init --recursive
    git pull --rebase
}

function install_rust()
{
    if [ ! -d "$RUSTUP_HOME" ]; then
        mkdir "$RUSTUP_HOME"
    fi

    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs -o "$RUSTUP_HOME/rustup.sh"
    chmod +x "$RUSTUP_HOME/rustup.sh"

    "$RUSTUP_HOME/rustup.sh" -y
    source "$CARGO_HOME/env"
}

function build_rust_demangle()
{
    git_init rustc-demangle https://github.com/alexcrichton/rustc-demangle.git main

    cargo build -p rustc-demangle-capi --release
    export RUST_DEMANGLE_LIB=$PWD/target/release/librustc_demangle.so
    return 0
}

function build_hotspot()
{
    git_init hotspot https://github.com/KDAB/hotspot.git master

    rm -Rf build
    mkdir build
    cd build

    CRAFT_PREFIX=/opt/craft/
    PREFIX=/usr
    cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo -GNinja \
        "-DCMAKE_PREFIX_PATH=$CRAFT_PREFIX" \
        -DAPPIMAGE_BUILD=ON "-DCMAKE_INSTALL_PREFIX=$PREFIX" ..

    DESTDIR=appdir ninja install

    # FIXME: Do in CMakeLists.txt
    mkdir -p "appdir/$PREFIX/share/applications/"
    cp ../com.kdab.hotspot.desktop "appdir/$PREFIX/share/applications/"

    # Ensure we prefer the bundled libs also when calling dlopen, cf.: https://github.com/KDAB/hotspot/issues/89
    cat << WRAPPER_SCRIPT > ./appdir/AppRun
#!/bin/bash
f="\$(readlink -f "\${0}")"
d="\$(dirname "\$f")"
bin="\$d/$PREFIX/bin"
unset QT_PLUGIN_PATH
LD_LIBRARY_PATH="\$d/$PREFIX/lib":\$LD_LIBRARY_PATH "\$bin/hotspot" "\$@"
WRAPPER_SCRIPT
    chmod +x ./appdir/AppRun

    # include breeze icons
    mkdir -p "appdir/$PREFIX/share/icons/breeze"
    cp -v "$CRAFT_PREFIX/share/icons/breeze/breeze-icons.rcc" "appdir/$PREFIX/share/icons/breeze/"

    linuxdeploy-x86_64.AppImage --appdir appdir --plugin qt \
        -e "./appdir/$PREFIX/lib64/libexec/hotspot-perfparser" \
        -e "./appdir/$PREFIX/bin/hotspot" \
        -l "$RUST_DEMANGLE_LIB" \
        -l "$CRAFT_PREFIX/lib/libz.so.1" \
        -l /usr/lib64/libharfbuzz.so.0 \
        -i ../src/images/icons/512-hotspot_app_icon.png --icon-filename=hotspot \
        -d "./appdir/$PREFIX/share/applications/com.kdab.hotspot.desktop" \
        --output appimage
    mv Hotspot-*-x86_64.AppImage ../../hotspot-$(git describe)-x86_64.AppImage
}

pushd .
install_rust
popd

pushd .
build_rust_demangle
popd

pushd .
build_hotspot
popd
