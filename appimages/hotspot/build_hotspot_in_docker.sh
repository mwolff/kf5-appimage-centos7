#!/bin/sh

set -e

cd $(dirname $0)

../../run_craft_in_docker.sh \
    kde/frameworks/tier1/kcoreaddons \
    kde/frameworks/tier1/ki18n \
    kde/frameworks/tier1/kitemmodels \
    kde/frameworks/tier1/kitemviews \
    kde/frameworks/tier1/threadweaver \
    kde/frameworks/tier1/solid \
    kde/frameworks/tier1/kwindowsystem \
    kde/frameworks/tier1/karchive \
    kde/frameworks/tier1/syntax-highlighting \
    kde/frameworks/tier3/knotifications \
    kde/frameworks/tier3/kconfigwidgets \
    kde/frameworks/tier3/kio \
    kde/frameworks/tier3/kiconthemes \
    kde/frameworks/tier3/kparts \
    elfutils libzstd linuxdeploy kddockwidgets

cp build.sh shared/
RUN_IN_DOCKER_EXTRA_ARGS="-v $PWD/shared:/opt/hotspot" \
    ../../run_in_docker.sh /opt/hotspot/build.sh
